
$(document).ready(function () {
    onPageLoading();
    // hiển thị Pizza trang đầu tiên
    makeCall(0)

    // Hàm xử lý vùng range price

    $("#slider-range").slider({
        range: true,
        values: [0, 50],
        min: 0,
        max: 50,
        step: 1,
        slide: function (event, ui) {
            $("#input-min-price").html(ui.values[0]);

            $("#input-max-price").html(ui.values[1]);
        }
    });

})

"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://64747ed27de100807b1b0fa4.mockapi.io/api/v1/";
var gBASE_URL_FOOD_ORDERING = "https://food-ordering-fvo9.onrender.com/api";
let gOrderPizza = [];
var gTotalRecords = 8;
var gPerpage = 9;
var gTotalPages = Math.ceil(gTotalRecords / gPerpage);
var vPagenum = 0;

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $('html, body').animate({
        scrollTop: $('#choose-pizza-section').offset().top
    }, 'slow');
    // Kiểm tra local Storage có sản phẩm hay không thì chuyển màu cho icon bag
    if (localStorage.getItem('Order Pizza') !== null) {
        gOrderPizza = JSON.parse(localStorage.getItem('Order Pizza'))
        $('.bi-handbag').removeAttr('style');
        $('.bi-handbag').css('color', 'yellow');
    } else {
        gOrderPizza = [];
        $('.bi-handbag').removeAttr('style');
        $('.bi-handbag').css('color', 'white');

    }

    // Gán sự kiện cho dấu cộng ở vùng Section HOME KITCHEN
    $('.container-card-pizza').on('click', '.plus-button', function (event) {
        var vSelectedPizza =
        {
            id: 0,
            name: '',
            imageUrl: '',
            price: 0,
            rating: '',
            quantity: 1,
            time: "",
        }
        var vPizzaId = $(this).attr('id')
        vSelectedPizza.id = Number(vPizzaId);

        var vParent = $(this).closest('.type-pizza-container');
        var vChildren = $(vParent).children()[0];

        var vImageUrl = ($(vChildren).children('img#image').prop('currentSrc'));
        vSelectedPizza.imageUrl = vImageUrl;

        var vDivChildren = ($(vChildren).children()[1]);
        var vPizzaNameAndPrice = $(vDivChildren).children('div.title-item')[0];

        var vDivPizzaName = $(vPizzaNameAndPrice).children('div#pizza-name');
        var vPizzaName = vDivPizzaName.prop('innerText');
        vSelectedPizza.name = vPizzaName;

        var vDivPizzaPrice = $(vPizzaNameAndPrice).children('div#card-price');
        var vPizzaPrice = vDivPizzaPrice.prop('innerText');
        vSelectedPizza.price = Number(vPizzaPrice.slice(1));

        var vClosetParent = $(this).closest('#div-wrapper')[0]
        var vDivReview = $(vClosetParent).children('div.review')[0]
        var vSpanReview = $(vDivReview).children()[0];
        var vFistSpanReview = $(vSpanReview).children('span.number-stars');
        var vReview = $(vFistSpanReview).text();
        vSelectedPizza.rating = vReview;

        var vDivTime = $(vClosetParent).children('div.time')[0]
        var vTime = $(vDivTime).children('span').text();
        vSelectedPizza.time = vTime;

        // Hàm Kiểm tra đơn hàng có trùng nhau hay không thì tăng số lượng
        checkQuantityOfOrder(vSelectedPizza);
        //Đổi màu cho button bag 
        $('.bi-handbag').removeAttr('style');
        $('.bi-handbag').css('color', 'yellow');
    })
    // Gán sự kiện cho button bag
    $('.header-nav-icon').on('click', '.bi-handbag', function () {
        // console.log('co chay')
        window.location.href = 'giohang.html';
    })
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Gọi API API lấy danh sách Pizza load ra trang 1
function callAPI(vPagenum, gPerpage, min) {
    const vQueryParams = new URLSearchParams();
    vQueryParams.append('_limit', 9);
    vQueryParams.append('_page', 0);

    if ($('#review-5stars').is(':checked')) {
        vQueryParams.append('rating', 5);
    }

    if ($('#review-4stars').is(':checked')) {
        vQueryParams.append('rating', 4);
    }

    if ($('#review-3stars').is(':checked')) {
        vQueryParams.append('rating', 3);
    }

    if ($('#review-2stars').is(':checked')) {
        vQueryParams.append('rating', 2);
    }

    if ($('#review-1stars').is(':checked')) {
        vQueryParams.append('rating', 1);
    }

    if ($('#input-min-price').text() !== 0 && $('#input - max - price').text() !== 0) {
        vQueryParams.append('priceMin', $('#input-min-price').text())
        vQueryParams.append('priceMax', $('#input-max-price').text())
    }

    $.ajax({
        type: 'get',
        url: 'https://food-ordering-fvo9.onrender.com/api/foods?' + vQueryParams.toString(),
        dataType: 'json',
        success: function (paramData) {
            var vPizzaObj = paramData.rows;
            console.log(vPizzaObj)
            displayData(vPizzaObj);
        },
        error: errorProcess
    });
}

// Hàm hiển thị pizza ra trang
function displayData(paramResponseText) {
    $('.container-card-pizza').html('');
    for (let index = 0; index < paramResponseText.length; index++) {
        $('.container-card-pizza').append(`
        <div class="col type-pizza-container">
            <div class="card h-100 div-kitchen-card">
                <img id='image' src=${paramResponseText[index].imageUrl} alt="...">
                <div class="card-body d-flex flex-column justify-content-between card-body-detail">
                    <div class="row title-item">
                        <div id='pizza-name' class="col-9">
                            ${paramResponseText[index].name}
                        </div>
                        <div id='card-price' class="col-3 text-center ">
                            $${paramResponseText[index].price} 
                        </div>
                    </div>
                    <div class="row align-items-center mt-3 card-item-info">
                        <div id='div-wrapper' class="col-12 d-flex align-items-center">
                            <div class="col-3 text-left mr-1 badge review">
                                <span><i class="bi bi-star-fill"></i><span> </span>
                                    <span class="number-stars">${paramResponseText[index].rating}</span>
                                </span>
                            </div>
                            <div class="col-4 badge text-left time">
                                <span>${paramResponseText[index].time}</span>
                            </div>
                            <div class="col-4 text-right plus"><button id='${paramResponseText[index].id}' type="button"
                                class="btn btn-warning btn-sm plus-button"><i
                                    class="bi bi-plus-lg "></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `)
    }
}

// Hàm xử lý khi gọi API thất bại
function errorProcess(paramError) {
    alert(paramError.responseText);
}

// Hàm update local storage
function updateLocalStorage() {
    var vString = JSON.stringify(gOrderPizza);
    localStorage.setItem('Order Pizza', vString);
}
// Hàm Kiểm tra đơn hàng có trùng nhau hay không thì tăng số lượng
function checkQuantityOfOrder(paramNewOrderPizza) {
    var vPizzaId = paramNewOrderPizza.id;
    let vFound = false;
    let index = 0;
    while (vFound === false && index < gOrderPizza.length) {
        if (gOrderPizza[index].id === vPizzaId) {
            gOrderPizza[index].quantity = gOrderPizza[index].quantity + 1;
            vFound = true;
        } else {
            index++;
        }
    }

    if (vFound === false) {
        gOrderPizza.push(paramNewOrderPizza);
    }

    updateLocalStorage();

    return gOrderPizza;

}
// Hàm thêm  thêm OrderPizza vào giỏ hàng chung
function addNewItemToList(paramNewOrderPizza) {
    gOrderPizza.push(paramNewOrderPizza);
}

// Hàm action cho mỗi nút phân trang
function makeCall(vPagenum) {
    createpagination(vPagenum);
    callAPI(vPagenum, gPerpage)
}

// Hàm tạo thanh phân trang
function createpagination(vPagenum) {
    $('#page-container').html('');

    // Hiển thị 5 trang trong phạm vi
    for (var index = 0; index <= 4; index++) {
        if (vPagenum === index) {
            $('#page-container').append(`
                <li class="page-item disabled">
                    <a class="page-link" href="javascript:void(0)" tabindex="-1">${index + 1}</a>
                </li>  
                `)


        } else {
            $('#page-container').append(`
                <li class="page-item" onclick='makeCall(${index})'>
                    <a class="page-link" href="javascript:void(0)">${index + 1}</a>
                </li>  
                `)
        }
    }



}


