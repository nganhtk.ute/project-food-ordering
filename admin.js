"use strict";

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://food-ordering-fvo9.onrender.com/api";
let gORDER = ['STT', 'orderCode', 'fullName', 'methodPayment', 'quantity', 'totalPrice', 'createAt', 'id']
var gSTT = 1;
const gSTT_COL = 0;
const gORDER_CODE_COL = 1;
const gFULL_NAME_COL = 2;
const gMETHOD_PAYMENT_COL = 3;
const gFOODS_COL = 4;
const gTOTAL_PRICE_COL = 5;
const gCREATE_DATE_COL = 6;
const gID_COL = 7;
let gORDER_LIST = [];
let gOrderId = '';
let gOrderTable = $('#table-order-list').DataTable({
    columns: [
        {
            data: gORDER[gSTT_COL],
            title: 'STT',

        },
        {
            data: gORDER[gORDER_CODE_COL],
            title: 'Mã đơn hàng',
        },
        {
            data: gORDER[gFULL_NAME_COL],
            title: 'Họ và tên',
        },
        {
            data: gORDER[gMETHOD_PAYMENT_COL],
            title: 'Payment Method',
        },
        {
            data: gORDER[gFOODS_COL],
            title: 'Số lượng sản phẩm',
        },
        {
            data: gORDER[gTOTAL_PRICE_COL],
            title: 'Tổng tiền',
        },
        {
            data: gORDER[gCREATE_DATE_COL],
            title: 'Ngày đặt hàng',
        },
        {
            data: gORDER[gID_COL],
            title: 'Action',
        },
    ],
    columnDefs: [
        {
            targets: gSTT_COL,
            class: "text-center",
            render: function () {
                return gSTT++;
            }
        },
        {
            targets: gFULL_NAME_COL,
            width: "17%",
        },
        {
            targets: gFOODS_COL,
            class: "text-center",
            width: "12%",
        },
        {
            targets: gTOTAL_PRICE_COL,
            class: "text-center",
        },
        {
            targets: gCREATE_DATE_COL,
            class: "text-center",
        },
        {
            targets: gID_COL,
            render: function (id, type) {
                if (type === "display") {
                    return `
                    <div class="d-flex">
                        <button data-id=${id} class="btn edit-order"><i class="fa-solid fa-file-pen" data-toggle="tooltip" data-placement="bottom" title="Sửa"></i></button>
                        <button data-id=${id} class="btn delete-order"><i class="fa-solid fa-trash" data-toggle="tooltip" data-placement="bottom" title="Xóa"></i></button>       
                    </div>
                `;
                }
                return id;
            }
        },

    ]
})

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
// Hàm xử lý sự kiện tải trang

function onPageLoading() {
    $(".overlay").show();
    callApiGetOrderList();
}
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm gọi api lấy danh sách đơn hàng 
function callApiGetOrderList() {
    $.ajax({
        url: gBASE_URL + "/orders",
        type: 'GET',
        success: function (paramSuccess) {

            orderList(paramSuccess);
            displayDataToTable();
            //Gán sự kiện cho nút sửa
            $('#table-order-list').on('click', '.edit-order', function () {

                gOrderId = Number(this.dataset.id);
                var vOrderObj = gORDER_LIST.find(function (paramOrder) {
                    return paramOrder.id === gOrderId;
                })
                console.log(vOrderObj);
                $('#input-edit-firstName').val(vOrderObj.firstName);
                $('#input-edit-lastName').val(vOrderObj.lastName);
                $('#input-edit-email').val(vOrderObj.email);
                $('#input-edit-phone').val(vOrderObj.phone);
                $('#input-edit-address').val(vOrderObj.address);
                $('#edit-select-payment').val(vOrderObj.methodPayment);
                // $('#input-edit-discount').val(vOrderObj.voucherId);
                var vVoucherId = vOrderObj.voucherId;
                if (vVoucherId !== null) {
                    $.ajax({
                        url: gBASE_URL + "/vouchers/" + vVoucherId,
                        type: 'GET',
                        success: function (paramSuccess) {
                            $('#input-edit-discount').val(paramSuccess.voucherCode);
                        },
                        error: function (paramError) {
                            console.log(paramError.responseText);
                        }
                    })
                }

                $('.modal-edit').modal('show');
            })
            //Gán sự kiện cho nút cập nhật (modal)
            $('#btn-edit').on('click', function () {


                processEditOrder();
            })
            //Gán sự kiện cho nút xóa
            $('#table-order-list').on('click', '.delete-order', function () {
                $('.modal-delete').modal('show');
                gOrderId = this.dataset.id
            })
            //Gán sự kiện cho nút xác nhận (modal)
            $('#btn-confirm').click(function () {
                $('.modal-delete').modal('hide');
                callApiDeleteOrder(gOrderId);
            })
        },
        error: function (paramError) {
            console.assert(paramError.responseText);
        }
    })

}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm xử lý lại orderList
function orderList(paramResponse) {

    for (var i = 0; i < paramResponse.length; i++) {
        var vOrderObj = {
            id: paramResponse[i].id,
            address: paramResponse[i].address,
            email: paramResponse[i].email,
            orderCode: paramResponse[i].orderCode,
            firstName: paramResponse[i].firstName,
            lastName: paramResponse[i].lastName,
            phone: paramResponse[i].phone,
            methodPayment: paramResponse[i].methodPayment,
            foods: paramResponse[i].foods,
            createAt: paramResponse[i].createdAt,
            updatedAt: paramResponse[i].updatedAt,
            voucherId: paramResponse[i].voucherId,
            fullName: '',
            quantity: 0,
            totalPrice: 0,
        };
        vOrderObj.fullName = paramResponse[i].firstName + " " + paramResponse[i].lastName;
        vOrderObj.quantity = paramResponse[i].foods.length;
        var vArrayFoods = vOrderObj.foods
        vOrderObj.totalPrice = calcTotalPrice(vArrayFoods);

        var vRawDate = paramResponse[i].createdAt;
        var vCreatedAtDate = new Date(vRawDate).toLocaleDateString('es-CL');
        vOrderObj.createAt = vCreatedAtDate;

        gORDER_LIST.push(vOrderObj);
    }

}
//Hàm tính tổng giá tiền
function calcTotalPrice(paramArrayFoods) {
    var result = 0;
    for (var index = 0; index < paramArrayFoods.length; index++) {
        result += Number(paramArrayFoods[index].price);
    }
    return result;
};
// Hàm tải đơn hàng vào table
function displayDataToTable() {
    gOrderTable.clear();
    gOrderTable.rows.add(gORDER_LIST);
    gOrderTable.draw();
    $('#table-order-list thead').css('background-color', '#1AC073');
    $('#table-order-list thead').addClass("text-light");
    $('#table-order-list thead').addClass("text-center");
    $('#table-order-list thead tr th').css("vertical-align", "middle");
    //Hàm chạy tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $(".overlay").hide();
}
// Hàm xử lý nút edit 
function processEditOrder() {
    var vUpdatedOrder = {
        firstName: "",
        lastName: "",
        email: "",
        address: "",
        phone: "",
        methodPayment: "",
        voucherId: "",
    };
    // Đọc dữ liệu trên modal

    vUpdatedOrder.firstName = $.trim($('#input-edit-firstName').val());
    vUpdatedOrder.lastName = $.trim($('#input-edit-lastName').val());
    vUpdatedOrder.email = $.trim($('#input-edit-email').val());
    vUpdatedOrder.phone = $.trim($('#input-edit-phone').val());
    vUpdatedOrder.address = $.trim($('#input-edit-address').val());
    vUpdatedOrder.methodPayment = $('#edit-select-payment').val();
    vUpdatedOrder.voucherId = $.trim($('#input-edit-discount').val());

    // Kiểm tra dữ liệu

    var dataIsValidated = checkData(vUpdatedOrder);
    if (dataIsValidated) {
        // gọi api update
        updateOrder(vUpdatedOrder);
    }
}

// Hàm kiểm tra dữ liệu 
function checkData(paramOrderObj) {
    let vIsValid = true;
    onCheckVoucherClick(vIsValid)
    if (paramOrderObj.firstName === '') {
        alert('Vui lòng nhập họ và đệm')
        vIsValid = false;
    }
    if (paramOrderObj.lastName === '') {
        alert('Vui lòng nhập tên')
        vIsValid = false;
    }
    if (paramOrderObj.email === '') {
        alert('Vui lòng nhập email')
        vIsValid = false;
    }
    var vEmailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
    if (vEmailRegex.test(paramOrderObj.email) === false) {
        alert('Email không đúng định dạng')
        vIsValid = false;
    }
    if (paramOrderObj.address === '') {
        alert('Vui lòng nhập address')
        vIsValid = false;
    }
    if (paramOrderObj.phone === '') {
        alert('Vui lòng nhập phone')
        vIsValid = false;
    }
    if (/^[0-9]{10}$/.test(paramOrderObj.phone) === false) {
        alert('Điện thoại phải là 10 chữ số');
        vIsValid = false;
    }
    if (paramOrderObj.methodPayment === '') {
        alert('Vui lòng chọn phương thức thanh toán')
        vIsValid = false;
    }

    return vIsValid;
}

// API kiểm tra voucher hợp lệ
function onCheckVoucherClick(paramValid) {
    var vVoucherCode = $('#input-edit-discount').val();
    $.ajax({
        url: gBASE_URL + "/vouchers" + "?" + 'voucherCode=' + vVoucherCode,
        type: "GET",
        success: function (paramSuccess) {
            if (!paramSuccess.voucherCode === vVoucherCode) {
                alert('Mã giảm giá không tồn tại');
                paramValid = false;
            } else {
                vVoucherCode = paramSuccess.voucherCode;
            }
        },
        error: function (paramError) {
            console.assert(paramError.responseText);
        }
    })

}
// API Post Order
function updateOrder(paramUpdateOrder) {
    var vVoucherCode = $('#input-edit-discount').val();
    console.log(vVoucherCode);
    $.ajax({
        url: gBASE_URL + "/vouchers" + "?" + 'voucherCode=' + vVoucherCode,
        type: "GET",
        success: function (paramSuccess) {
            console.log(paramSuccess);
            paramUpdateOrder.voucherId = paramSuccess[0].id;
            console.log(paramUpdateOrder);
            $.ajax({
                url: gBASE_URL + "/orders/" + gOrderId,
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify(paramUpdateOrder),
                success: function (paramSuccess) {
                    // console.log(paramSuccess);
                    alert('Update đơn hàng thành công')
                    $('.modal-edit').html('');
                    $('.modal-edit').modal('hide');
                    location.reload();
                }
            })
        },
        error: function (paramError) {
            console.assert(paramError.responseText);
        }
    })
}

// API xóa 1 Order theo Id
function callApiDeleteOrder(paramOrderId) {
    console.log(paramOrderId)
    $.ajax({
        url: gBASE_URL + "/orders/" + paramOrderId,
        type: "DELETE",
        success: function (paramSuccess) {
            console.log(paramSuccess);
            alert('Xóa đơn hàng thành công')
            location.reload();
        }
    })
}

