
$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gBASE_URL = "https://64747ed27de100807b1b0fa4.mockapi.io/api/v1/";
    var gBASE_URL_FOOD_ORDERING = "https://food-ordering-fvo9.onrender.com/api";
    let gOrderPizza = [];
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        $(".overlay").show();

        // Khi người dùng cuộn từ 20px thì sẽ hiện nút top để scroll về đầu trang
        window.onscroll = function () { scrollFunction() };

        // Kiểm tra local Storage có sản phẩm hay không thì chuyển màu cho icon bag
        if (localStorage.getItem('Order Pizza') !== null) {
            gOrderPizza = JSON.parse(localStorage.getItem('Order Pizza'))
            $('.bi-handbag').removeAttr('style');
            $('.bi-handbag').css('color', 'yellow');
        } else {
            gOrderPizza = [];
            $('.bi-handbag').removeAttr('style');
            $('.bi-handbag').css('color', 'white');

        }
        // call Api lấy danh sách Pizza và Load data to card
        callApiGetPizzaList()
        // call Api lấy danh sách Blog và Load data to card
        callApiGetBlogsList()

        // Gán sự kiện cho dấu cộng ở vùng Section HOME KITCHEN
        $('.container-card-pizza').on('click', '.plus-button', function (event) {
            var vSelectedPizza =
            {
                id: 0,
                name: '',
                imageUrl: '',
                price: 0,
                rating: '',
                quantity: 1,
                time: "",
            }
            var vPizzaId = $(this).attr('id')
            vSelectedPizza.id = Number(vPizzaId);

            var vParent = $(this).closest('.type-pizza-container');
            var vChildren = $(vParent).children()[0];

            var vImageUrl = ($(vChildren).children('img#image').prop('currentSrc'));
            vSelectedPizza.imageUrl = vImageUrl;

            var vDivChildren = ($(vChildren).children()[1]);
            var vPizzaNameAndPrice = $(vDivChildren).children('div.title-item')[0];

            var vDivPizzaName = $(vPizzaNameAndPrice).children('div#pizza-name');
            var vPizzaName = vDivPizzaName.prop('innerText');
            vSelectedPizza.name = vPizzaName;

            var vDivPizzaPrice = $(vPizzaNameAndPrice).children('div#card-price');
            var vPizzaPrice = vDivPizzaPrice.prop('innerText');
            vSelectedPizza.price = Number(vPizzaPrice.slice(1));

            var vClosetParent = $(this).closest('#div-wrapper')[0]
            var vDivReview = $(vClosetParent).children('div.review')[0]
            var vSpanReview = $(vDivReview).children()[0];
            var vFistSpanReview = $(vSpanReview).children('span.number-stars');
            var vReview = $(vFistSpanReview).text();
            vSelectedPizza.rating = vReview;

            var vDivTime = $(vClosetParent).children('div.time')[0]
            var vTime = $(vDivTime).children('span').text();
            vSelectedPizza.time = vTime;

            // Hàm Kiểm tra đơn hàng có trùng nhau hay không thì tăng số lượng
            checkQuantityOfOrder(vSelectedPizza);
            //Đổi màu cho button bag 
            $('.bi-handbag').removeAttr('style');
            $('.bi-handbag').css('color', 'yellow');
        })
        // Gán sự kiện cho button bag
        $('.header-nav-icon').on('click', '.bi-handbag', function () {
            // console.log('co chay')
            window.location.href = 'giohang.html';
        })
        $(".overlay").hide();
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // API lấy danh sách Pizza
    function callApiGetPizzaList() {
        $.ajax({
            async: true,
            url: gBASE_URL + "/pizza",
            type: "GET",
            success: displayData,
            error: errorProcess,
        })
    }

    // Hàm callback: xử lý khi thành công
    function displayData(paramResponseText) {
        // console.log(paramResponseText)
        for (let index = 0; index < paramResponseText.length; index++) {
            $('.container-card-pizza').append(`
        <div class="col type-pizza-container">
            <div class="card h-100 div-kitchen-card">
                <img id='image' src=${paramResponseText[index].imageUrl} alt="...">
                <div class="card-body d-flex flex-column justify-content-between card-body-detail">
                    <div class="row title-item">
                        <div id='pizza-name' class="col-9">
                            ${paramResponseText[index].name}
                        </div>
                        <div id='card-price' class="col-3 text-center ">
                            $${paramResponseText[index].price} 
                        </div>
                    </div>
                    <div class="row align-items-center mt-3 card-item-info">
                        <div id='div-wrapper' class="col-12 d-flex align-items-center">
                            <div class="col-3 text-left mr-1 badge review">
                                <span><i class="bi bi-star-fill"></i><span> </span>
                                    <span class="number-stars">${paramResponseText[index].rating}</span>
                                </span>
                            </div>
                            <div class="col-4 badge text-left time">
                                <span>${paramResponseText[index].time}</span>
                            </div>
                            <div class="col-4 text-right plus"><button id='${paramResponseText[index].id}' type="button"
                                class="btn btn-warning btn-sm plus-button"><i
                                    class="bi bi-plus-lg "></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `)
        }
    }

    // Hàm callback: xử lý khi thất bại
    function errorProcess(paramError) {
        alert(paramError.responseText);
    }

    //Hàm call Api lấy danh sách blog
    function callApiGetBlogsList() {
        $.ajax({
            async: true,
            url: gBASE_URL + "/blogs",
            type: "GET",
            success: displayBlog,
            error: errorProcess,
        })
    }
    // Hàm hiển thị dữ liệu Blod ra trang web
    function displayBlog(paramResponseText) {
        // console.log(paramResponseText)
        for (let index = 0; index < paramResponseText.length; index++) {
            if (paramResponseText[index].id === "1") {
                $('.main-blog').append(`
                <div class="card detail-main-blog">
                    <img src=${paramResponseText[index].imageUrl} class="main-card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="main-card-title">${paramResponseText[index].title}</h5>
                        <p class="main-card-text">${paramResponseText[index].description}</p>
                    </div>
                </div>
            `)
                continue;
            }

            if (Number(paramResponseText[index].id) % 2 === 0) {
                $(".left-side-blogs").append(`
                <div class="col blog-item left-detail-blog">
                    <div class="card h-100">
                        <img src=${paramResponseText[index].imageUrl} class="left-card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="left-card-title">${paramResponseText[index].title}</h5>
                        <p class="left-card-text">${paramResponseText[index].description}</p>
                    </div>
                    </div>
                </div>
            `)
            } else {
                $(".right-side-blogs").append(`
                <div class="col blog-item right-detail-blog ">
                    <div class="card h-100">
                        <img src=${paramResponseText[index].imageUrl} class="right-card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="right-card-title">${paramResponseText[index].title}</h5>
                        <p class="right-card-text">${paramResponseText[index].description}</p>
                    </div>
                    </div>
                </div>
            `)
            }
        }
    }

    // Hàm update local storage
    function updateLocalStorage() {
        var vString = JSON.stringify(gOrderPizza);
        localStorage.setItem('Order Pizza', vString);
    }
    // Hàm Kiểm tra đơn hàng có trùng nhau hay không thì tăng số lượng
    function checkQuantityOfOrder(paramNewOrderPizza) {
        // // 1. kiem tra xem pizza co trong gio hang chua
        // const index = gOrderPizza.findIndex(pizza => pizza.id === paramNewOrderPizza.id);

        // if (index >= 0) {
        //     gOrderPizza[index].quantity += 1;
        // } else {
        //     paramNewOrderPizza.quantity = 1
        //     gOrderPizza.push(paramNewOrderPizza);
        // }
        // updateLocalStorage();
        var vPizzaId = paramNewOrderPizza.id;
        let vFound = false;
        let index = 0;
        while (vFound === false && index < gOrderPizza.length) {
            if (gOrderPizza[index].id === vPizzaId) {
                gOrderPizza[index].quantity = gOrderPizza[index].quantity + 1;
                vFound = true;
            } else {
                index++;
            }
        }

        if (vFound === false) {
            gOrderPizza.push(paramNewOrderPizza);
        }

        updateLocalStorage();

        return gOrderPizza;

        // if (gOrderPizza.length === 0) {
        //     addNewItemToList(paramNewOrderPizza);
        //     updateLocalStorage();
        // } else {
        //     for (var index = 0; index < gOrderPizza.length; index++) {
        //         if (gOrderPizza[index].id === paramNewOrderPizza.id) {
        //             gOrderPizza[index].quantity++;
        //             updateLocalStorage();
        //         } else {
        //             addNewItemToList(paramNewOrderPizza)
        //             updateLocalStorage();
        //         }
        //     }
        // }

    }
    // Hàm thêm  thêm OrderPizza vào giỏ hàng chung
    function addNewItemToList(paramNewOrderPizza) {
        gOrderPizza.push(paramNewOrderPizza);
    }

    // Hàm xử lý sự kiện khi người dùng nhấn nút top để quay lại đầu trang
    $("#btn-scroll").click(function () {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    })

    // Hàm xử lý xuất hiện nút top quay lại đầu trang
    function scrollFunction() {
        let vMyScrollbutton = $("#btn-scroll")
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            vMyScrollbutton.css("display", "block")
        } else {
            vMyScrollbutton.css("display", "none");
        }
    }
})

